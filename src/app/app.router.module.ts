import {RouterModule, Routes} from '@angular/router';
import {SubscribersListComponent} from './components/subscribers-list/subscribers-list.component';
import {NgModule} from '@angular/core';
import {SubscriberDetailComponent} from './components/subscriber-detail/subscriber-detail.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: SubscribersListComponent
  },
  {
    path: 'subscriber/:msisdn',
    component: SubscriberDetailComponent
  },
  {
    path: '**',
    component: SubscribersListComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRouterModule {
}
