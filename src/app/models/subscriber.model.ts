export interface ISubscriber {
  msisdn: string;
  surname: string;
  documentType?: 'Паспорт' | 'Загран паспорт' | 'Паспорт моряка';
  countryOfIssue?: string;
  dateOfIssue?: Date;
  seriesAndNumber?: string;
  divisionCode?: string;
  issuingAuthority?: string;
  registrationAddress?: string;
}
