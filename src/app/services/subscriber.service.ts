import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ISubscriber} from '../models/subscriber.model';

@Injectable()
export class SubscriberService {

  constructor(private _httpService: HttpService) {
  }

  getSubscribersList(): Observable<ISubscriber[]> {
    return this._httpService.get('subscriber/list')
      .pipe(
        map(res => {
            if (!res.success) {
              console.log('error in req');
              return [];
            }
            return res.data;
          }
        )
      );
  }

  getSubscriber(msisdn: string): Observable<ISubscriber> {
    return this._httpService.get(`subscriber/${msisdn}`)
      .pipe(
        map(res => {
          if (!res.success) {
            console.log('error in req');
            return {};
          }
          return res.data;
        })
      );
  }


}
