import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

const url = 'http://127.0.0.1:4000';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  post(path: string, body): Observable<any> {
    return this.http.post<Observable<any>>(`${url}/${path}`, body, httpOptions);
  }

  get(path: string): Observable<any> {
    return this.http.get<Observable<any>>(`${url}/${path}`, httpOptions);
  }

  delete(path: string, params): Observable<any> {
    return this.http.delete<Observable<any>>(`${url}/${path}`, {...httpOptions, params: params});
  }

  put(path: string, body, params): Observable<any> {
    return this.http.put<Observable<any>>(`${url}/${path}`, body, {...httpOptions, params: params});
  }
}
