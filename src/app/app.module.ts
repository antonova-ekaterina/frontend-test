import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRouterModule} from './app.router.module';
import {SubscribersListComponent} from './components/subscribers-list/subscribers-list.component';
import {AppMaterialModule} from './app.material.module';
import {SubscriberService} from './services/subscriber.service';
import {HttpClientModule} from '@angular/common/http';
import {SubscriberDetailComponent} from './components/subscriber-detail/subscriber-detail.component';
import {RouterModule} from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import {ReactiveFormsModule} from '@angular/forms';
import {ModalDialogComponent} from './components/modal-dialog/modal-dialog.component';

import {TextMaskModule} from 'angular2-text-mask';

@NgModule({
  declarations: [
    AppComponent,
    SubscribersListComponent,
    SubscriberDetailComponent,
    ModalDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRouterModule,
    AppMaterialModule,
    HttpClientModule,
    MatMenuModule,
    ReactiveFormsModule,
    TextMaskModule
  ],
  entryComponents: [ModalDialogComponent],
  providers: [SubscriberService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
