import {Component, OnInit} from '@angular/core';
import {SubscriberService} from '../../services/subscriber.service';
import {ISubscriber} from '../../models/subscriber.model';

@Component({
  selector: 'app-subscribers-list',
  templateUrl: 'subscribers-list.component.html',
  styleUrls: ['subscribers-list.component.css']
})
export class SubscribersListComponent implements OnInit {

  subscribers: ISubscriber[] = [];

  constructor(private _subscriberService: SubscriberService) {
  }

  ngOnInit() {
    this._subscriberService.getSubscribersList()
      .subscribe((subscribers: ISubscriber[]) => {
        this.subscribers = subscribers;
        console.log(subscribers);
      });
  }
}
