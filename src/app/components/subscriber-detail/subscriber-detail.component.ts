import {Component, OnInit} from '@angular/core';
import {SubscriberService} from '../../services/subscriber.service';
import {ActivatedRoute} from '@angular/router';
import {ISubscriber} from '../../models/subscriber.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {ModalDialogComponent} from '../modal-dialog/modal-dialog.component';
import {dateValidator, forbiddenNameValidator} from '../../../shared/forbidden-name.directive';

@Component({
  selector: 'app-subscriber-detail',
  templateUrl: 'subscriber-detail.component.html',
  styleUrls: ['subscriber-detail.component.css']
})
export class SubscriberDetailComponent implements OnInit {

  public dateMask = [/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/];
  public seriesAndNumberMask = [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  subscriber: ISubscriber;
  userForm: FormGroup;
  documentType: string;
  docTypeAccord = [{
    name: 'passport',
    value: 'Паспорт'
  }, {
    name: 'international',
    value: 'Загран паспорт'
  }, {
    name: 'sailor',
    value: 'Паспорт моряка'
  }];

  visibleCopyDocType = false;

  constructor(private _activatedRoute: ActivatedRoute,
              private _subscriberService: SubscriberService,
              private _fb: FormBuilder,
              public dialog: MatDialog) {

    this.userForm = this._fb.group({
      surname: ['', [Validators.required, forbiddenNameValidator('ru')]],
      documentType: ['', Validators.required],
      countryOfIssue: ['', [Validators.required, forbiddenNameValidator('ru')]],
      dateOfIssue: ['', [Validators.required, dateValidator()]],
      seriesAndNumber: ['', [Validators.required, forbiddenNameValidator('number')]],
      divisionCode: ['', [Validators.required, forbiddenNameValidator('number')]],
      issuingAuthority: ['', [Validators.required, forbiddenNameValidator('ru punctuation')]],
      registrationAddress: ['', [Validators.required, forbiddenNameValidator('ru punctuation number')]]
    });
  }

  ngOnInit() {
    const msisdn = this._activatedRoute.snapshot.paramMap.get('msisdn');
    this._subscriberService.getSubscriber(msisdn)
      .subscribe((subscriber: ISubscriber) => {
        this.subscriber = subscriber;
        this.documentType = this.docTypeAccord.filter(elem => {
          if (elem.value === this.subscriber.documentType) {
            return elem;
          }
        })[0].name;
      });

  }

  interrupt() {
    this.dialog.open(ModalDialogComponent, {
      data: {
        massage: 'Сценарий прерван'
      }
    });
  }

  addToBasket() {
    this.dialog.open(ModalDialogComponent, {
      data: {
        massage: 'Сценарий добавлен в корзину'
      }
    });
  }

  putDate(date): string {
    const day = date.substr(8, 2);
    const month = date.substr(5, 2);
    const year = date.substr(0, 4);
    return day + '.' + month + '.' + year;
  }

}
