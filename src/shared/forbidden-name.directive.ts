import {AbstractControl, ValidatorFn} from '@angular/forms';

const ru: string[] = [
  'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о',
  'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
];

const punctuation: string[] = [
  ' ', ',', '.', '?', '!'
];

export function forbiddenNameValidator(validationType: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let forbidden = false;


    if (validationType === 'number') {
      for (let i = 0; i < control.value.length; i++) {
        if (!Number.parseInt(control.value[i])) {
          forbidden = true;
        }
      }
    }

    if (validationType === 'ru') {
      for (let i = 0; i < control.value.length; i++) {
        if (ru.indexOf(control.value[i].toLowerCase()) === -1) {
          forbidden = true;
        }
      }
    }

    if (validationType === 'ru punctuation') {
      for (let i = 0; i < control.value.length; i++) {
        if (ru.indexOf(control.value[i].toLowerCase()) === -1
          && punctuation.indexOf(control.value[i]) === -1) {
          forbidden = true;
        }
      }
    }

    if (validationType === 'ru punctuation number') {
      for (let i = 0; i < control.value.length; i++) {
        if (!Number.parseInt(control.value[i])
          && ru.indexOf(control.value[i].toLowerCase()) === -1
          && punctuation.indexOf(control.value[i]) === -1) {
          forbidden = true;
        }
      }
    }
    return forbidden ? {'forbiddenName': {value: control.value}} : null;
  };
}

export function dateValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let forbidden = false;
    const date = new Date(
      control.value.substr(6, 4),
      control.value.substr(3, 2) - 1,
      control.value.substr(0, 2)
    );
    const compareDate = new Date(1991, 0, 1);
    if (compareDate.getTime() > date.getTime()) {
      forbidden = true;
    }
    // console.log('forbidden = ', forbidden);
    return forbidden ? {'forbiddenName': {value: control.value}} : null;
  };
}
